OCR Kata Written for TestDouble

Kata Repo:
https://github.com/testdouble/contributing-tests/wiki/Bank-OCR-kata

To run the tests:
* download Repo from git
* Copy filepath to ./test/test_runner.html
* Paste filepath into chrome to execute tests
* Click on tests to expand results to inspect as needed

Notes on work completed
* Total working time was 2hrs and 25 minutes. I spent about 40 minutes refamiliarizing myself with JavaScript and getting the test suite installed and working
* I did not have time to implement a user interface or IO interpreter.
* The Mocha tests are able to execute individual account numbers.
* Code and tests were implemented up to a partially implemented User Story 4.  
* I implemented a partial test file based on each user story test examples in the kata readme. With more time I would have ported all the tests into JavaScript

Next Steps for Kata
* Improve on the account/digit design pattern.  I was running into some technical debt with how I was storing digits in the account number and how I was initializing and ended up doing more procedural code than I'd like mostly due to my lack of familiarity with JavaScript Object Oriented design patterns.
* Implement an interface. I wanted to put in a simple UI where a user can upload a .txt file with as many account numbers as they wanted and then print out results in <p>s but I way ran out of time and didn't want to cheat.
* Improve testing. Port all existing test cases in the kata Readme into this test suite to make sure I am capturing all predetermined edge cases.
