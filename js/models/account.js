function Account(rowOne, rowTwo, rowThree) {
  this.rowOne = rowOne;
  this.rowTwo = rowTwo;
  this.rowThree = rowThree;
  this.digits = [];
  this.interpretedAccountNumber = '';

  this.interpretAccountNumber = function() {

    // Check sum uses a reverse index, start at 9 and go down.
    var checkSumMultiplier = 9;
    // initialize array of digits from iterations of 3 chars in each row
    for (var i = 0; i < 26; i = i + 3 ) {
      var digitRowOne = this.rowOne.slice(i, i + 3);
      var digitRowTwo = this.rowTwo.slice(i, i + 3);
      var digitRowThree = this.rowThree.slice(i, i + 3);
      var digit = new Digit(digitRowOne, digitRowTwo, digitRowThree, checkSumMultiplier);

      // TODO Pull this into a function that uses the digits array to create multiplier
      // Appends digit # to account number.

      // Push created digit into digits array
      digit.interpretLines();
      this.digits.push(digit);
      checkSumMultiplier--
    }
    this.interpretedAccountNumber = this.findNumberFromCurrentDigits();

    this.performCheckSum();

    if(this.status == ' ERR' || this.status == ' ILL') {
      var alternatives = this.deterineAlternatives();
      if(alternatives.length == 1) {
        this.interpretedAccountNumber = alternatives[0];
      } else if(alternatives.length > 1) {
        this.status = ' AMB';
        this.interpretedAccountNumber = this.interpretedAccountNumber +
                                        this.status + " " +
                                        alternatives.join(", ");
      }
    }

    return this.interpretedAccountNumber;
  }

  this.performCheckSum = function() {
    if(this.interpretedAccountNumber.length === 0) {
      this.interpretAccountNumber();
    }
    // Add checksum value from each digit to perform check
    var sum = this.sumDigits(this.digits);

    this.status = this.determineStatus(this.interpretedAccountNumber, sum);
    return this.interpretedAccountNumber;
  }

  this.deterineAlternatives = function() {
    var alternatives = [];
    for (var digit of this.digits) {
      for(var alternativeDigit of digit.findAlternatives()) {
        digit.interpretedNumber = alternativeDigit;
        var number = this.findNumberFromCurrentDigits()
        if(this.determineStatus(number, this.sumDigits()) == '') {
          alternatives.push(number);
        }
        // reset digit;
        digit.interpretLines()
      }
    }
    return alternatives;
  }

// private
  this.determineStatus = function(accountNumber, sum) {
    if(accountNumber.indexOf('?') > -1) {
      return ' ILL';
    } else if (sum % 11 == 0) {
      return '';
    } else {
      return ' ERR'
    }
  }

  this.sumDigits = function() {
    var sum = 0;
    for (var i in this.digits) {
      var digit = this.digits[i];
      sum = sum + digit.checkSumValue();
    }
    return sum;
  }

  this.findNumberFromCurrentDigits = function() {
    var number = '';
    for(var digit of this.digits) {
      number = number + digit.interpretedNumber;
    }
    return number;
  }

  this.statusOk = function() {
    this.status == '';
  }
}
