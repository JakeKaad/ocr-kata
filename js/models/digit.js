function Digit(rowOne, rowTwo, rowThree, checkSumMultiplier) {
  this.rowOne = rowOne;
  this.rowTwo = rowTwo;
  this.rowThree = rowThree;
  this.interpretedNumber = null;
  this.checkSumMultiplier = checkSumMultiplier;

  this.NUMBERS = [
    //0
    ' _ | ||_|',
    //1
    '     |  |',
    //2
    ' _  _||_ ',
    //3
    ' _  _| _|',
    //4
    '   |_|  |',
    //5
    ' _ |_  _|',
    //6
    ' _ |_ |_|',
    //7
    ' _   |  |',
    //8
    ' _ |_||_|',
    //9
    ' _ |_| _|'

  ]

  this.interpretLines = function(){
    var result = this.NUMBERS.indexOf(this.concatenatedRows())
    if(result === -1) {
      this.interpretedNumber = '?';
    } else {
      this.interpretedNumber = result;
    }
    return this.interpretedNumber;
  };

  this.checkSumValue = function() {
    return this.interpretedNumber * this.checkSumMultiplier;
  }

  this.findAlternatives = function() {
    // finds alternative #s
    var alternatives = [];
    for (var i = 0; i < 9; i++) {
      var options = [' ', '|', '_'];
      for (var option of options) {
        var chars = this.concatenatedRows().split("");
        // Don't check if the char is already the option
        if(option !== chars[i]) {
          chars[i] = option;
          var result = this.NUMBERS.indexOf(chars.join(""));
          if(result > -1) {
            alternatives.push(result);
          }
        }
      }
    }

    return alternatives;
  };


// Private
  this.concatenatedRows = function() {
    return rowOne + rowTwo + rowThree;
  };
};
