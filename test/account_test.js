describe('Account', function() {
  // Digit tests provide enough confidence that digits can print determine themselves
  // These tests just ensure proper integration with digits.
  describe('#interpretAccountNumber()', function() {
    it("interprets and returns 000000000", function() {
      //  _  _  _  _  _  _  _  _  _
      // | || || || || || || || || |
      // |_||_||_||_||_||_||_||_||_|
      var row_one   = ' _  _  _  _  _  _  _  _  _ ';
      var row_two   = '| || || || || || || || || |';
      var row_three = '|_||_||_||_||_||_||_||_||_|';
      var account = new Account(row_one, row_two, row_three);
      expect(account.interpretAccountNumber()).to.equal("000000000");
    });

    it("interprets and returns 666666666", function() {
      //  _  _  _  _  _  _  _  _  _
      // |_ |_ |_ |_ |_ |_ |_ |_ |_
      // |_||_||_||_||_||_||_||_||_|
      var row_one   = ' _  _  _  _  _  _  _  _  _ ';
      var row_two   = '|_ |_ |_ |_ |_ |_ |_ |_ |_ ';
      var row_three = '|_||_||_||_||_||_||_||_||_|';
      var account = new Account(row_one, row_two, row_three);
      expect(account.interpretAccountNumber()).to.equal("666666666 AMB 686666666, 666566666");
    });

    it("interprets and returns 123456789", function() {
      //    _  _     _  _  _  _  _
      //  | _| _||_||_ |_   ||_||_|
      //  ||_  _|  | _||_|  ||_| _|
      var row_one   = '    _  _     _  _  _  _  _ ';
      var row_two   = '  | _| _||_||_ |_   ||_||_|';
      var row_three = '  ||_  _|  | _||_|  ||_| _|';
      var account = new Account(row_one, row_two, row_three);
      expect(account.interpretAccountNumber()).to.equal("123456789");
    });

    it("when number is 111111111 it interprets 711111111", function() {
      //
      //  |  |  |  |  |  |  |  |  |
      //  |  |  |  |  |  |  |  |  |
      var row_one   = '                           ';
      var row_two   = '  |  |  |  |  |  |  |  |  |';
      var row_three = '  |  |  |  |  |  |  |  |  |';
      var account = new Account(row_one, row_two, row_three);
      expect(account.interpretAccountNumber()).to.equal("711111111");
    });
  });

  describe('#performCheckSum()', function() {
    it("when number is 000000051 it is valid", function() {
      // _  _  _  _  _  _  _  _
      //| || || || || || || ||_   |
      //|_||_||_||_||_||_||_| _|  |
      var row_one   = ' _  _  _  _  _  _  _  _    ';
      var row_two   = '| || || || || || || ||_   |';
      var row_three = '|_||_||_||_||_||_||_| _|  |';
      var account = new Account(row_one, row_two, row_three);
      expect(account.performCheckSum()).to.equal("000000051");
    });

    it("when number is 49006771? it is invalid and marked with ILL", function() {
      //    _  _  _  _  _  _     _
      //|_||_|| || ||_   |  |  | _
      //  | _||_||_||_|  |  |  | _|
      var row_one   = '    _  _  _  _  _  _     _ ';
      var row_two   = '|_||_|| || ||_   |  |  | _ ';
      var row_three = '  | _||_||_||_|  |  |  | _|';
      var account = new Account(row_one, row_two, row_three);
      account.performCheckSum()
      expect(account.status).to.equal(" ILL");
    });

    it("when number is 000000057 it is invalid and marked with ERR", function() {
      // _  _  _  _  _  _  _  _  _
      //| || || || || || || ||_   |
      //|_||_||_||_||_||_||_| _|  |
      var row_one   = ' _  _  _  _  _  _  _  _  _ ';
      var row_two   = '| || || || || || || ||_   |';
      var row_three = '|_||_||_||_||_||_||_| _|  |';
      var account = new Account(row_one, row_two, row_three);
      account.performCheckSum()
      expect(account.status).to.equal(" ERR");
    });
  });


  describe('#deterineAlternatives()', function() {
    it("when number is 711111111 it returns [111111111]", function() {
      //
      //  |  |  |  |  |  |  |  |  |
      //  |  |  |  |  |  |  |  |  |
      var row_one   = '                           ';
      var row_two   = '  |  |  |  |  |  |  |  |  |';
      var row_three = '  |  |  |  |  |  |  |  |  |';
      var account = new Account(row_one, row_two, row_three);
      account.interpretAccountNumber();
      expect(account.deterineAlternatives().join()).to.equal("711111111");
    });
  });
});
