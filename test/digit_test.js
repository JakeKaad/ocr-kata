describe('Digit', function() {
  describe('#interpretLines()', function() {
    it("interprets and returns ? when invalid", function() {
      // _ //
      //  |//
      //|_|//
      var digit = new Digit(' _ ', '  |', '|_|');
      expect(digit.interpretLines()).to.equal('?');
    });

    it("interprets and returns 0", function() {
      // _ //
      //| |//
      //|_|//
      var digit = new Digit(' _ ', '| |', '|_|');
      expect(digit.interpretLines()).to.equal(0);
    });

    it("interprets and returns 1", function() {
      //   //
      //  |//
      //  |//
      var digit = new Digit('   ', '  |', '  |');
      expect(digit.interpretLines()).to.equal(1);
    });

    it("interprets and returns 2", function() {
      // _ //
      // _|//
      //|_ //
      var digit = new Digit(' _ ', ' _|', '|_ ');
      expect(digit.interpretLines()).to.equal(2);
    });

    it("interprets and returns 3", function() {
      // _ //
      // _|//
      // _|//
      var digit = new Digit(' _ ', ' _|', ' _|');
      expect(digit.interpretLines()).to.equal(3);
    });

    it("interprets and returns 4", function() {
      //   //
      //|_|//
      //  |//
      var digit = new Digit('   ', '|_|', '  |');
      expect(digit.interpretLines()).to.equal(4);
    });

    it("interprets and returns 5", function() {
      // _ //
      //|_ //
      // _|//
      var digit = new Digit(' _ ', '|_ ', ' _|');
      expect(digit.interpretLines()).to.equal(5);
    });

    it("interprets and returns 6", function() {
      // _ //
      //|_ //
      //|_|//
      var digit = new Digit(' _ ', '|_ ', '|_|');
      expect(digit.interpretLines()).to.equal(6);
    });

    it("interprets and returns 7", function() {
      // _ //
      //  |//
      //  |//
      var digit = new Digit(' _ ', '  |', '  |');
      expect(digit.interpretLines()).to.equal(7);
    });

    it("interprets and returns 8", function() {
      // _ //
      //|_|//
      //|_|//
      var digit = new Digit(' _ ', '|_|', '|_|');
      expect(digit.interpretLines()).to.equal(8);
    });

    it("interprets and returns 9", function() {
      // _ //
      //|_|//
      // _|//
      var digit = new Digit(' _ ', '|_|', ' _|');
      expect(digit.interpretLines()).to.equal(9);
    });
  });

  describe('#checkSumValue()', function(){
    it("interprets and returns 9 when checkSumMultiplier is 1", function() {
      // _ //
      //|_|//
      // _|//
      var digit = new Digit(' _ ', '|_|', ' _|', 1);
      digit.interpretLines();
      expect(digit.checkSumValue()).to.equal(9);
    });

    it("interprets and returns 18 when checkSumMultiplier is 2", function() {
      // _ //
      //|_|//
      // _|//
      var digit = new Digit(' _ ', '|_|', ' _|', 2);
      digit.interpretLines();
      expect(digit.checkSumValue()).to.equal(18);
    });
  });

  describe('#findAlternatives()', function(){
    it("it finds alternative digit 7 for 1", function() {
      //   //
      //  |//
      //  |//
      var digit = new Digit('   ', '  |', '  |', 1);
      expect(digit.findAlternatives().join()).to.equal("7");
    });

    it("it finds alternative digits [3, 5, 8] for 9", function() {
      // _ //
      //|_|//
      // _|//
      var digit = new Digit(' _ ', '|_|', ' _|', 1);
      expect(digit.findAlternatives().join()).to.equal("3,5,8");
    });
  });
});
